#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// TODO: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
home ** readhomes(char * filename)
{
    // Create inital array of 50 pointers
    int arrlen = 50;
    home **arr = (home **)malloc(arrlen * sizeof(home *));
    // Read in file, expanding array as you go
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        printf("Can't open %s\n", filename);
        exit(1);
    }
    int zip = 0;
    char addr[50];
    int price = 0;
    int area = 0;
    int i = 0;
    while(fscanf(f, "%d,%49[^,],%d,%d", &zip,addr,&price,&area) != EOF)
    {
        // initialize each element in the array.
        arr[i] = malloc(sizeof(home));        
        arr[i]->addr = (char*)malloc((strlen(addr)+1) * sizeof(char));
        strcpy(arr[i]->addr, addr);
        arr[i]->zip = zip;        
        arr[i]->price = price;
        arr[i]->area = area;
        i++;
        if (i == arrlen)
        {
            // Make array 25% bigger
            arrlen = (int)(arrlen * 1.25);
            home **newarr = realloc(arr, arrlen * sizeof(home *));
            if (newarr != NULL) arr = newarr;
            else exit(3);
        }
    }
    
    // Add NULL to end of array to mark the end
    arr[i] = NULL;
    
    return arr;
}

void printarr(home ** h)
{
  int i = 0;
  while (h[i] != NULL)
  {
    printf("printarr - zip: %d\n Addr: %s\n Price: %d\n Area: %d\n", h[i]->zip, h[i]->addr, h[i]->price, h[i]->area);
    i++;
  }
}

int NumberOfHomes(home ** h)
{
  int i = 0;
  while (h[i] != NULL)
  {
    i++;
  }
  return i;
}

int FindHomesByZip(int zip, home ** h)
{
  int i = 0;
  int count = 0;
  while (h[i] != NULL)
  {
    if (h[i]->zip == zip)
      count++;
    i++;
  }
  return count;
}

void PrintHomesFromPrice(int price, home ** h, int range)
{
  int i = 0;
  while (h[i] != NULL)
  {
    if ((price >= h[i]->price - range) && (price <= h[i]->price + range))
      printf("Address: %s. Price: %d \n", h[i]->addr, h[i]->price);
    i++;
  }
}

int main(int argc, char *argv[])
{
    // TODO: Use readhomes to get the array of structs
    if (argc < 2) 
    {
        printf("Usage: %s csv_file\n", argv[0]);
        exit(1);
    }
    home ** test;
    test = readhomes(argv[1]);
    //printarr(test);
    // At this point, you should have the data structure built
    // TODO: How many homes are there? Write a function, call it,
    // and print the answer.
    int noOfHomes = NumberOfHomes(test);
    printf("There are %d homes\n", noOfHomes);
    // TODO: Prompt  the user to type in a zip code.
    int zip = 0;
    printf("Please enter in a zip code to search for.\n");
    scanf("%d", &zip);
    
    // At this point, the user's zip code should be in a variable
    // TODO: How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    int homesByZip = FindHomesByZip(zip, test);
    printf("There are exactly %d homes in the zip code of %d\n", homesByZip, zip);

    // TODO: Prompt the user to type in a price.
    int price = 0;    
    printf("Please enter in a price:\n");
    scanf("%d", &price); 


    // At this point, the user's price should be in a variable
    // TODO: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    PrintHomesFromPrice(price, test, 10000); 
}
